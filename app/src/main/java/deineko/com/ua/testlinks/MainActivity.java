package deineko.com.ua.testlinks;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final EditText editText = (EditText) findViewById(R.id.editText);
        Button button = (Button) findViewById(R.id.button);
        final WebView myWebView = (WebView) findViewById(R.id.webview);
        //myWebView.loadUrl("http://www.example.com");
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
               String url = editText.getText().toString();
                myWebView.loadUrl(url);
            }
        });
    }
}
